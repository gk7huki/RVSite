<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>RVGL Project Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="RVGL - Cross-Platform Re-Volt Port."/>
<meta name="keywords" content="revolt, re-volt, acclaim, iguana, probe, entertainment, we, go, interactive, rc, r/c, car, racing, games, download, patch, update, windows, xp, vista, 7, 8, compatibility, linux, ubuntu, mint, mac, osx, ios, ipad, iphone, android, version, 1.2, v1.2, rvgl, rvzt, rv12, 1207, 0916, music, cd, rip"/>
<link rel="stylesheet" type="text/css" href="rvgl.css"/>
<link rel="stylesheet" type="text/css" href="layouts/layout-index.css"/>
<link rel="stylesheet" href="rvio-header.css">
<link rel="icon" href="imgs/icon.png" type="image/png"/>
</head>
