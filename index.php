<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<?php include "site.php"; ?>
<?php include "head.php"; ?>

<body>

  <?php include "rvio-header.html"; ?>

  <!-- The wrapper is used for the grid layout, includes all content blocks 
       The actual layout is defined in the layout.css file using css grids.
       The page is backwards compatible to browsers that don't support css
       grids (very few today). To try it you, comment out the 
       layout.css link in the header.
  -->

  <div class="section wrapper">

    <div id="top" class="section header">
      <h1>Welcome to the <span class="red">RVGL</span> project page</h1>
      <div class="navlist">
        <a href="#download">Download</a> | 
        <a href="#extras">Extras</a> | 
        <a href="#news">News</a> | 
        <a href="#features">Features</a> | 
        <a href="#screenshots">Screenshots</a> | 
        <a href="#videos">Videos</a>
      </div>
    </div>

    <!-- List of used libraries =========================================== -->

    <div id="libraries" class="section libraries">
      <img src="imgs/rvgl_logo.png">
      <p>
        RVGL is a cross-platform rewrite / port of <a href="https://en.wikipedia.org/wiki/Re-Volt"><strong>Re-Volt</strong></a> that runs natively on a wide variety of platforms. It's powered entirely by modern, open source components. We currently use:
      </p>
      <div class="box">
        <p>
          The industry standard <a href="https://www.opengl.org/"><span class="blue bold">OpenGL</span></a> for the graphics. Both legacy and modern OpenGL devices are supported.
        </p>
      </div>
      <div class="box">
        <p>
          <a href="https://openal-soft.org/"><span class="yellow bold">OpenAL Soft</span></a> for the audio. This is an independent, software implementation of OpenAL that provides various backends for different platforms. It also supports audio streaming facilities (OGG, FLAC, MP3).
        </p>
      </div>
      <div class="box">
        <p>
          <a href="https://www.libsdl.org/"><span class="dark-blue bold">SDL2</span></a> for input - keyboard and various joysticks, gamepads and controllers, and other platform specific support (threads, timers, etc).
        </p>
      </div>
      <div class="box">
        <p>
          <a href="http://enet.bespin.org/"><span class="green bold">ENet</span></a>, a lightweight UDP library for networking.
        </p>
      </div>
    </div>

    <!-- Game downloads =================================================== -->
    
    <div id="launcher" class="section launcher">
      <h2>RVGL Launcher</h2>
      <p class="notice">The recommended way to install RVGL is using RVGL Launcher, a cross-platform launcher and package manager.</p>
      <p><img src="https://re-volt.gitlab.io/rvgl-launcher/screens/install.th.png"></p>
      <div class="dlbuttons">
        <a class="dlbutton" href="https://re-volt.gitlab.io/rvgl-launcher/" target="_blank">Download</a>
      </div>
    </div>

    <div class="section download-info">
      <p class="notice">
        <span class="bold">Find out more about RVGL</span>: <a href="downloads/rvgl_changelog.txt" target="_blank">Changelog</a> | <a href="https://re-volt.gitlab.io/rvgl-docs/" target="_blank">Documentation</a> | <a href="https://forum.rvgl.org/viewtopic.php?f=8&t=13" target="_blank">Support</a><br/>
        You can report bugs and suggestions on <a href="https://forum.rvgl.org"><em>The Re-Volt Hideout</em></a> forum.
      </p>
    </div>

    <div id="download" class="section download">
      <h2>Download</h2>
      <p class="notice">
        These are patch-only downloads of RVGL. <span class="bold">You need the original game data to use these patches.</span> Get the game data and soundtrack from <a href="https://re-volt.io/downloads/misc" target="_blank">Re-Volt I/O</a>. Pre-built <a href="https://re-volt.io/downloads/game" target="_blank">full game packages</a> are available too.
      </p>
      <p class="vernum">Current version: <?php echo $rvgl_build; ?></p>
      <p>
        <span class="bold">Setup:</span>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_setup_win32.exe">Windows (32-bit)</a>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_setup_win64.exe">Windows (64-bit)</a>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_macos.dmg">macOS</a>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_android.apk">Android</a>
      </p>
      <p>
        <span class="bold">Portable:</span>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_win32.7z">Windows (32-bit)</a>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_win64.7z">Windows (64-bit)</a>
        <a class="dlbutton-small" href="downloads/rvgl_<?php echo $rvgl_build; ?>_linux.7z">GNU/Linux</a>
      </p>
      <p>
        <span class="bold">Other Platforms:</span>
        <a class="dlbutton-small" href="https://forum.odroid.com/viewtopic.php?f=91&t=20689" target="_blank">ODROID</a>
        <a class="dlbutton-small" href="https://repo.openpandora.org/?page=detail&app=rvgl_ptitseb" target="_blank">OpenPandora</a>
      </p>
    </div>

    <!-- Extras section (additional downloads/versions, etc.) ============= -->

    <div id="extras" class="section extras">
      <h2>Extras</h2>
      <p class="notice">
        <span class="bold">Dreamcast Pack</span>: This pack includes Rooftops and 14 additional cars. Get it <a href="downloads/rvgl_dcpack.7z">here</a>.
      </p>
      <p class="notice">
        <span class="bold">Controller Map</span>: Android app that allows generating SDL Game Controller mappings. Get it <a href="downloads/rvgl_controllermap.apk">here</a>.
      </p>
    </div>

    <!-- News section ===================================================== -->

    <div id="news" class="section news"><h2>News</h2></div>

    <!-- Left column of the news section ================================== -->

    <div id="news" class="section news-one">
      <h2>RVGL featured on Kotaku</h2>
      <p><b><a href="https://www.kotaku.com.au/2015/10/of-all-things-re-volt-is-still-being-patched/" target="_blank">Of All Things, Re-Volt Is Still Being Patched</a> (27 Oct 2015)<br/>
      <i>"...it's incredibly heartwarming to see something as good as Re-Volt to be supported, not only for so long, but so competently as well."</i></b></p>
      <p>"It's the remote control racer that was pushed out by Acclaim's Cheltenham studio  in 1999. The fans have been keeping the game alive through hell or high water since 2004, working through the wilderness and the hazards of having a mobile developer pick up the IP in 2010." <a href="https://www.kotaku.com.au/2015/10/of-all-things-re-volt-is-still-being-patched/" target="_blank">[Read more]</a></p>

      <h2>RVGL featured on PandoraLive</h2>
      <p><b><a href="https://web.archive.org/web/20161208101329/http://pandoralive.info/?p=5485" target="_blank">RVGL Makes Racing Fun on the Pandora</a> (01 Mar 2016)<br/>
      <i>"Probably the best racing game you will ever get on Pandora."</i></b></p>
      <p>"...even though the game is, frankly, old, it still looks great to this day. I'm not sure I can say the same thing of many other games from 1999, but that's a testament to the great design the original developers were capable of, with limited polygon count and texture space at their disposal. And kudos to the RVGL team which made it possible to run this game on OpenGL, paving the way for a Pandora port." <a href="https://web.archive.org/web/20161208101329/http://pandoralive.info/?p=5485" target="_blank">[Read more]</a></p>
    </div>

    <!-- Right column of the news section ================================= -->

    <div id="news" class="section news-two">
      <h2>RVGL in the ODROID Magazine</h2>
      <p><b><a href="https://magazine.odroid.com/201604" target="_blank">RVGL - Re-Volt on OpenGL</a> (April 2016 issue, p24)<br/>
      <i>"The multiplayer mode is especially fun, and having the option to play on split screen on the same ODROID makes it even better."</i></b></p>
      <div class="img-right"><a href="https://magazine.odroid.com/wp-content/uploads/ODROID-Magazine-201604.pdf" target="_blank"><img height="150" src="https://magazine.odroid.com/wp-content/uploads/ODROID-Magazine-201604-221x300.png" alt=""/></a></div>
      <p>"Re-Volt is a racing game like no other. Rather than racing big fancy cars against friends on tracks like the famous Nürnburgring, or racing karts on fantasy tracks, you instead race with radio controlled (RC) cars. This gives the game a very unique perspective, since you play from the point of view of tiny RC cars in all sorts of fun courses such as supermarkets and backyards. [...] RVGL is a reimplementation of Re-Volt in OpenGL for modern PCs. The RV-Team, and especially Huki and jigebren, have done a lot of work to get this 17-year old game to work on modern platforms and were nice enough to allow us to make a port for ODROIDs too." <a href="https://magazine.odroid.com/201604" target="_blank">[Read more]</a></p>

      <h2>RVGL on the Re-Volt I/O blog</h2>
      <p><b><a href="https://re-volt.io/blog/rvgl-on-the-pandora" target="_blank">RVGL on the Pandora</a> (06 Mar 2016)</b></p>
      <p>"The combined effort of ptitSeb and Huki has brought us a port of our beloved game to the Open Pandora handheld. It is based on a recent version of RVGL and it works just like any other version of the game." <a href="https://re-volt.io/blog/rvgl-on-the-pandora" target="_blank">[Read more]</a></p>

      <p><b><a href="https://re-volt.io/blog/rvgl-first-alpha-release" target="_blank">RVGL - First Alpha Release!</a> (02 Mar 2015)</b></p>
      <p>"RVGL is Re-Volt redone with open source tools! It works on Windows and Linux natively. It's pretty exciting. If you wanna take Re-Volt to the next level, go here and give it a go." <a href="https://re-volt.io/blog/rvgl-first-alpha-release" target="_blank">[Read more]</a></p>
    </div>

    <!-- Features section ================================================= -->

    <div id="features" class="section features">
      <h2>Features</h2>

      <p class="bold">What's new in RVGL:</p>
      <ul>
        <li>Support for various platforms (Windows, Linux, macOS, Android) across multiple architectures.</li>
        <li>New renderer based on programmable shaders.</li>
        <li>Wide screen, Full HD, 4K resolutions support.</li>
        <li>Additional content from the Dreamcast version brought to PC and Android.</li>
        <li>2-4 player Split-Screen multiplayer mode brought to PC.</li>
        <li>Graphics: Improved font, native support for Anisotropic Filtering and MSAA.</li>
        <li>Audio: True 3D surround audio, original soundtrack playback (Ogg, Flac, MP3). </li>
        <li>Input: Supports a wide variety of gamepads, Xbox and PS4 controllers.</li>
        <li>Netplay: Low latency Peer-to-Peer multiplayer, spectating and lobby support.</li>
        <li>Unicode support, additional languages and International Keyboard Layouts support.</li>
        <li>Customization: car skins, per-car box art, per-level music, extensive features for custom content creation.</li>
        <li>Improved AI and improved gameplay stability, especially on high end systems.</li>
        <li>Efficient multi-threaded loading, silent loading and much more...</li>
      </ul>

      <h3>Graphics</h3>
      <div class="img-right">
        <a href="screenshots/widescreen.jpg">
          <img height="85" src="screenshots/widescreen.th.jpg" alt="Toytanic 1">
        </a>
      </div>
      <p>
        RVGL uses the cross-platform OpenGL API to bring Re-Volt alive in rich, high resolution graphics. Some of the supported features are automatic aspect ratio correction, resizeable windowed mode and gamma correction in fullscreen. Further, various graphical options including Anisotropic Texture Filtering and Antialiasing are natively supported. RVGL is also capable of automatically generating mipmap levels that help improve performance. Check these options out in <em>Options -> Video Settings</em>.
      </p>
      <p>
        Further, the game now utilizes a new rendering engine based on programmable shaders. This shader-based rendering engine is expected to have significantly improved performance on modern GPUs and better power efficiency on laptops and mobile devices.
      </p>
      <p class="notice">Pro Tip: Press the F11 key to toggle between Fullscreen and Windowed mode at any time.</p>
      <p class="notice">Pro Tip: Take screenshots by pressing the F8 key. Screenshots are saved in the game's profiles folder.</p>

      <h3>Split-Screen</h3>
      <div class="img-right">
        <a href="screenshots/splitscreen.jpg">
          <img height="100" src="screenshots/splitscreen.th.jpg" alt="Split-Screen">
        </a>
      </div>
      <p>
        We bring you 2 to 4 player Split-Screen multiplayer support to PC, previously exclusive to Console versions of the game! Configure up to 4 different controllers, or share the keyboard. Number Pad is assigned for Player 2 by default, allowing you to play with your friend with no prior configuration. That's not all - you can have CPU cars playing along with your friends in Split-Screen mode! Enable them from <em>Game Settings -> Multiplayer CPU</em>.
      </p>
      <p class="notice"  style="margin-top: 2em">Pro Tip: Once you and your friends have finished, use the Accelerate / Reverse keys to spectate CPU cars - <em>watch 'em lose</em>.</p>

      <h3>Multi-Player</h3>
      <p>
        RVGL features a low-latency Peer-to-Peer Multi-Player mode built on <a href="http://enet.bespin.org/" target="_blank">ENet</a>, with optional Multicast support. Race (or Tag) with up to 15 other players from anywhere in the world, with 8 additional slots for spectators. We use UDP Hole-Punching to ensure that players behind home routers are able to play together without additional configuration. The game abstracts away the complexities of NAT so that players from the same Local Network can play, at the same time, with other players from around the world!
      </p>
      <div class="caption-img img-left">
        <a href="screenshots/waitingroom.jpg">
          <img height="120" src="screenshots/waitingroom.th.jpg" alt="In-Game Lobby">
        </a>
        <p>In-Game Lobby</p>
      </div>

      <p>
        A mention about RVGL's Multi-Player support wouldn't be complete without Spectator mode and Late Joining support. That's right - jump right into an on-going game and get ready to race when the host restarts the game. Multi-Player chat messaging is improved with an in-game lobby - accessible at any time by hitting the TAB key, and support for International Keyboard Layouts. Type with <em>élan</em> - dead keys and AltGr are supported natively. 
      </p>
      <p>
        RVGL's Multi-Player support is fully cross-platform - this means players on Windows, Linux or an Android mobile device will be able to play together.
      </p>
      <p class="notice" style="margin-top: 2em;">Pro Tip: Type a quick message while racing by pressing the F12 key.</p>
      <p class="notice">Hosting a LAN party? <a href="https://discord.gg/NMT4Xdb" target="_blank">Join us on Discord</a> and let us know!</p>

      <h3>Original Soundtrack</h3>
      <p>
        RVGL is capable of playing the original soundtrack from hard disk. When downloading <a href="https://re-volt.io/downloads/misc" target="_blank">game assets</a>, be sure to get the package that includes soundtrack. If you happen to have an original Re-Volt CD in your possession, you can rip audio tracks in one of the supported formats (Ogg, Flac, MP3 or uncompressed WAV), and place them in the game's redbook folder as tracks numbered from track02[.ogg] to track15[.ogg]. Check the <a href="https://re-volt.gitlab.io/rvgl-docs/general.html#soundtrack" target="_blank">Documentation</a> for more info.
      </p>

      <h3>Additional Content</h3>
      <div class="caption-img img-right">
        <a href="screenshots/dreamcast.jpg">
          <img height="75" src="screenshots/dreamcast.th.jpg" alt="Dreamcast Content">
        </a>
        <p>Rooftops</p>
      </div>
      <p>
        Content exclusive to the Dreamcast version of the game are an optional integration to RVGL. The Dreamcast Pack, available from the Extras section, brings the Rooftops level and 14 additional cars, including BigVolt and BossVolt. Some of these cars come with alternate skins that resemble their PlayStation 1 counterparts! Choose between them from the Car Preview screen.
      </p>
      <p class="notice" style="margin-top: 2.8em;">
        If you grew up playing Re-Volt on Dreamcast, you'll find more info in our <a href="https://re-volt.gitlab.io/rvgl-docs/general.html#dreamcast-mode" target="_blank">Documentation</a> on how you can relive the Dreamcast experience with RVGL.
      </p>

      <h3>Custom Content</h3>
      <p>
        Tons of user-made cars and levels have been made for Re-Volt over the decade, and RVGL remains compatible with all of them. Check out the top-rated content on <a href="https://www.revoltworld.net/toplist/" target="_blank">Re-Volt World</a>. RVGL has greatly extended the possibilities to allow user-made content to not only match, but supersede the original. Consult the <a href="https://re-volt.gitlab.io/rvgl-docs/advanced.html" target="_blank">Documentation</a> for an exhaustive list of all options available for track and car makers. Below is a glimpse of what's possible with RVGL:
      </p>
      <p><strong>For Levels:</strong></p>
      <div class="caption-img img-right">
        <a href="screenshots/customlevel.jpg">
          <img height="100" src="screenshots/customlevel.th.jpg" alt="Kadish Sprint">
        </a>
        <p>Kadish Sprint</p>
      </div>
      <ul>
        <li>High resolution textures (up to 8192x8192) and multiple texture formats (BMP, PNG, JPG).</li>
        <li>Alpha Transparency through textures (both 32-bit BMP and PNG supported).</li>
        <li>Advanced customization (animated objects, graphics, music and sound effects).</li>
        <li>Custom Battle Tag and Stunt Arena levels and support for Practice and Time Trial modes.</li>
        <li>Various limits extended to support larger, more complex levels.</li>
        <li>Improved in-game development environment.</li>
      </ul>

      <p><strong>For Cars:</strong></p>
      <div class="caption-img img-right">
        <a href="screenshots/customcar.jpg">
          <img height="100" src="screenshots/customcar.th.jpg" alt="Naval Baron">
        </a>
        <p>Naval Baron</p>
      </div>
      <ul>
        <li>High resolution textures (see above).</li>
        <li>Support for per-car box art, shadows and car engine sounds.</li>
        <li>Statistics display in frontend.</li>
        <li>Special effects: author flying (like UFO) and upside-down-drivable cars (like Rotor).</li>
        <li>Animations: have parts turning with the wheel, like Panga!
        <li>Customizable Hood and Rearview camera for each car.</li>
      </ul>
      <p class="notice">Rich user-made content is produced these days in <a href="https://www.blender.org/" target="_blank">Blender</a>. There is a WIP Blender plugin <a href="https://yethiel.github.io/re-volt-addon/" target="_blank">available from here</a>.</p>

      <h3>Advanced options</h3>
      <p>
        We support various Launch Parameters that advanced users might find useful. You can find a full list in our <a href="https://re-volt.gitlab.io/rvgl-docs/general.html#launch-parameters" target="_blank">Documentation</a>.
      </p>
      <ul>
        <li><strong>-nointro</strong> lets you skip the splash screens (but you want to watch them, right?).</li>
        <li><strong>-sload</strong> lets you skip the loading screen (i.e., silent load).</li>
        <li><strong>-window &lt;width&gt; &lt;height&gt;</strong> starts the game in Windowed mode. The width and height are optional.</li>
        <li><strong>-aspect &lt;width&gt; &lt;height&gt; &lt;lens&gt;</strong> lets you change the display aspect ratio and field-of-view.</li>
        <li><strong>-noshader</strong> lets you use the legacy fixed pipeline renderer.</li>
        <li><strong>-nouser</strong> lets you disable user-made content.</li>
        <li><strong>-pass &lt;phrase&gt;</strong> protects your online session with a password.</li>
        <li><strong>-sessionlog</strong> saves your online race results in CSV format.</li>
      </ul>
    </div>

    <!-- Screenshots section ============================================== -->

    <div id="screenshots" class="section screenshots">
      <h2>Screenshots</h2>

      <h3>Windows, GNU/Linux</h3>
        <div class="displaybox">
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-01.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-01.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-02.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-02.th.jpg" alt=""/>
            </a>
          </div>
                  <div class="dbox-item">
            <a href="screenshots/screenshot-pc-03.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-03.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-04.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-04.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-05.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-05.th.jpg" alt=""/>
            </a>
          </div>
          <!-- edit mode screenshot? <div class="dbox-item">
            <a href="screenshots/screenshot-pc-06.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-06.th.jpg" alt=""/>
            </a>
          </div> -->
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-07.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-07.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-08.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-08.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-09.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-09.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pc-10.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pc-10.th.jpg" alt=""/>
            </a>
          </div>
        </div>

      <h3>Android</h3>
        <div class="displaybox">
          <div class="dbox-item">
            <a href="screenshots/screenshot-android-01.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-android-01.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-android-02.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-android-02.th.jpg" alt=""/>
            </a>
          </div>
          <!-- <div class="dbox-item">
            <a href="screenshots/screenshot-android-03.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-android-03.th.jpg" alt=""/>
            </a>
          </div> -->
          <div class="dbox-item">
            <a href="screenshots/screenshot-android-04.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-android-04.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-android-05.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-android-05.th.jpg" alt=""/>
            </a>
          </div>
        </div>

      <h3>OpenPandora</h3>
        <div class="displaybox">
          <div class="dbox-item">
            <a href="screenshots/screenshot-pandora-01.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pandora-01.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pandora-02.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pandora-02.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pandora-03.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pandora-03.th.jpg" alt=""/>
            </a>
          </div>
          <div class="dbox-item">
            <a href="screenshots/screenshot-pandora-04.jpg" target="_blank">
              <img class="dbox-image" src="screenshots/screenshot-pandora-04.th.jpg" alt=""/>
            </a>
          </div>
        </div>
    </div>

    <!-- Videos section =================================================== -->

    <div id="videos" class="section videos">
      <h2>Videos</h2>

      <div class="displaybox">

        <div class="dbox-item">
          <a href="https://youtu.be/jZvugBUGpzc" target="_blank">
            <img width="240" height="180" src="https://img.youtube.com/vi/jZvugBUGpzc/0.jpg" alt=""/>
          </a>
          <p>RVGL featured on Kotaku</p>
        </div>
        <div class="dbox-item">
          <a href="https://youtu.be/EMA3gPtrdsc" target="_blank">
            <img width="240" height="180" src="https://img.youtube.com/vi/EMA3gPtrdsc/0.jpg" alt=""/>
          </a>
          <p>RVGL running on the OpenPandora</p>
        </div>
        <div class="dbox-item">
          <a href="https://www.youtube.com/watch?v=vG0xN5Eih10&index=1&list=PL4Sz7_l-PtwCFguCwX2rc6BwmgsRkUELg" target="_blank">
            <img width="240" height="180" src="https://img.youtube.com/vi/vG0xN5Eih10/0.jpg" alt=""/>
          </a>
          <p>Nerd<sup>3</sup> Completes Re-Volt (RVGL)</p>
        </div>
        <div class="dbox-item">
          <a href="https://www.youtube.com/watch?v=v66gXNDcCM8&list=PLpM2X3mJT56xK7oChb3VYa2eSu_OD4FEG&t=0s&index=1" target="_blank">
            <img width="240" height="180" src="https://img.youtube.com/vi/v66gXNDcCM8/0.jpg" alt=""/>
          </a>
          <p>Rookie Championship November 2017</p>
        </div>
        <div class="dbox-item">
          <a href="https://www.youtube.com/watch?v=fIaRtvsFovw" target="_blank">
            <img width="240" height="180" src="https://img.youtube.com/vi/fIaRtvsFovw/0.jpg" alt=""/>
          </a>
          <p>Re-Volt - Rc Racing Mayhem</p>
        </div>
        <div class="dbox-item">
          <a href="https://www.youtube.com/watch?v=UcRErkTHEwQ" target="_blank">
            <img width="240" height="180" src="https://img.youtube.com/vi/UcRErkTHEwQ/0.jpg" alt=""/>
          </a>
          <p>RVGL Gameplay (Split-Screen)</p>
        </div>

      </div>
    </div>

    <!-- Footer =========================================================== -->
    
    <?php include "footer.php"; ?>

  </div>

</body>
</html>
