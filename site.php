<?php
  $rvgl_build = "23.1030a1";
  $rv_house_build = "0.94.6";
  $year = "2023";

  if (count(get_included_files()) > 1) {
    return;
  }
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>RVGL - Site Variables</title>
<link rel="icon" href="imgs/icon.png" type="image/png"/>
</head>

<body>

  <h3>Site Variables</h3>

  <p>RVGL Build: <?php echo $rvgl_build; ?></p>
  <p>RV House Build: <?php echo $rv_house_build; ?></p>
  <p>Year: <?php echo $year; ?></p>

</body>
</html>
