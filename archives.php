<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<?php include "site.php"; ?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>The RV Team Archives</title>
<link rel="icon" href="imgs/icon.png" type="image/png"/>
</head>

<?php
  function entry_win32_win64_setup_linux_android_macos($build) {
    $root = 'https://rvgl.org/downloads/';
    echo 'rvgl_'.$build.' [ '.
        'win32 (<a href="'.$root.'rvgl_'.$build.'_setup_win32.exe" target="_blank">setup</a> . <a href="'.$root.'rvgl_'.$build.'_win32.7z" target="_blank">7z</a>) | '.
        'win64 (<a href="'.$root.'rvgl_'.$build.'_setup_win64.exe" target="_blank">setup</a> . <a href="'.$root.'rvgl_'.$build.'_win64.7z" target="_blank">7z</a>) | '.
        '<a href="'.$root.'rvgl_'.$build.'_linux.7z" target="_blank">linux</a> | <a href="'.$root.'rvgl_'.$build.'_macos.dmg" target="_blank">macos</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_android.apk" target="_blank">android</a> ] <br/>';
  }
  function entry_win32_win64_setup_linux_android($build) {
    $root = 'https://rvgl.org/downloads/';
    echo 'rvgl_'.$build.' [ '.
        'win32 (<a href="'.$root.'rvgl_'.$build.'_setup_win32.exe" target="_blank">setup</a> . <a href="'.$root.'rvgl_'.$build.'_win32.7z" target="_blank">7z</a>) | '.
        'win64 (<a href="'.$root.'rvgl_'.$build.'_setup_win64.exe" target="_blank">setup</a> . <a href="'.$root.'rvgl_'.$build.'_win64.7z" target="_blank">7z</a>) | '.
        '<a href="'.$root.'rvgl_'.$build.'_linux.7z" target="_blank">linux</a> | <a href="'.$root.'rvgl_'.$build.'_android.apk" target="_blank">android</a> ] <br/>';
  }
  function entry_win32_win64_linux_android($build) {
    $root = 'https://rvgl.org/downloads/';
    echo 'rvgl_'.$build.' [ <a href="'.$root.'rvgl_'.$build.'_win32.7z" target="_blank">win32</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_win64.7z" target="_blank">win64</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_linux.7z" target="_blank">linux</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_android.apk" target="_blank">android</a> ] <br/>';
  }
  function entry_win32_win64_setup_linux($build) {
    $root = 'http://rv12.revoltzone.net/downloads/';
    echo 'rvgl_'.$build.' [ '.
        'win32 (<a href="'.$root.'rvgl_'.$build.'_setup_win32.exe" target="_blank">setup</a> . <a href="'.$root.'rvgl_'.$build.'_win32.7z" target="_blank">7z</a>) | '.
        'win64 (<a href="'.$root.'rvgl_'.$build.'_setup_win64.exe" target="_blank">setup</a> . <a href="'.$root.'rvgl_'.$build.'_win64.7z" target="_blank">7z</a>) | '.
        '<a href="'.$root.'rvgl_'.$build.'_linux.7z" target="_blank">linux</a> ] <br/>';
  }
  function entry_win32_win64_linux($build) {
    $root = 'http://rv12.revoltzone.net/downloads/';
    echo 'rvgl_'.$build.' [ <a href="'.$root.'rvgl_'.$build.'_win32.7z" target="_blank">win32</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_win64.7z" target="_blank">win64</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_linux.7z" target="_blank">linux</a> ] <br/>';
  }
  function entry_win32_linux($build) {
    $root = 'http://rv12.revoltzone.net/downloads/';
    echo 'rvgl_'.$build.' [ <a href="'.$root.'rvgl_'.$build.'_win32.7z" target="_blank">win32</a> | '.
        '<a href="'.$root.'rvgl_'.$build.'_linux.7z" target="_blank">linux</a> ] <br/>';
  }
?>

<body>
  <h2>Welcome to the RV Team Archives</h2>
  <p>Click <a href="index.php">here</a> to go back to the home page.</p>

  <h2>RVGL</h2>
  <p>
    <a href="downloads/rvgl_changelog.txt" target="_blank">Changelog</a> |
    <a href="https://re-volt.gitlab.io/rvgl-docs" target="_blank">Documentation</a> |
    <a href="https://forum.rvgl.org/viewtopic.php?f=8&t=13" target="_blank">Support</a> |
    <a href="downloads/rvgl_dcpack.7z" target="_blank">Dreamcast Pack</a> |
    <a href="downloads/rvgl_controllermap.apk" target="_blank">Controller Map</a>
  </p>
  <p>
  <?php
    entry_win32_win64_setup_linux_android_macos("23.1030a1");
    entry_win32_win64_setup_linux_android_macos("23.0602a1");
    entry_win32_win64_setup_linux_android_macos("23.0501a2");
    entry_win32_win64_setup_linux_android_macos("23.0501a1");
    entry_win32_win64_setup_linux_android_macos("21.0930a1");
    entry_win32_win64_setup_linux_android_macos("21.0905a1");
    entry_win32_win64_setup_linux_android_macos("21.0125a");
    entry_win32_win64_setup_linux_android("20.1230a");
    entry_win32_win64_setup_linux_android("20.0930a");
    entry_win32_win64_setup_linux_android("20.0905a");
    entry_win32_win64_setup_linux_android("20.0430a");
    entry_win32_win64_setup_linux_android("20.0325a");
    entry_win32_win64_setup_linux_android("20.0210a");
    entry_win32_win64_setup_linux_android("19.1230a");
    entry_win32_win64_setup_linux_android("19.1001a");
    entry_win32_win64_setup_linux_android("19.0907a");
    entry_win32_win64_setup_linux_android("19.0819a");
    entry_win32_win64_setup_linux_android("19.0430a");
    entry_win32_win64_setup_linux_android("19.0414a");
    entry_win32_win64_setup_linux_android("19.0330a");
    entry_win32_win64_setup_linux_android("19.0320a");
    entry_win32_win64_setup_linux_android("19.0301a");
    entry_win32_win64_setup_linux_android("19.0210a");
    entry_win32_win64_setup_linux_android("19.0120a");
    entry_win32_win64_setup_linux_android("18.1126a");
    entry_win32_win64_setup_linux_android("18.1112a");
    entry_win32_win64_setup_linux_android("18.1110a");
    entry_win32_win64_setup_linux_android("18.1020a");
    entry_win32_win64_setup_linux_android("18.0731a");
    entry_win32_win64_setup_linux_android("18.0725a");
    entry_win32_win64_setup_linux_android("18.0720a");
    entry_win32_win64_setup_linux_android("18.0514a");
    entry_win32_win64_setup_linux_android("18.0504a");
    entry_win32_win64_setup_linux_android("18.0428a");
    entry_win32_win64_setup_linux_android("18.0416a");
    entry_win32_win64_setup_linux_android("18.0410a");
    entry_win32_win64_setup_linux_android("18.0330a");
    entry_win32_win64_setup_linux_android("18.0315a");
    entry_win32_win64_setup_linux_android("18.0310a");
    entry_win32_win64_setup_linux("17.1222a");
    entry_win32_win64_setup_linux("17.1218a");
    entry_win32_win64_setup_linux("17.1124a");
    entry_win32_win64_setup_linux("17.1021a");
    entry_win32_win64_setup_linux("17.1014a");
    entry_win32_win64_setup_linux("17.1012a");
    entry_win32_win64_setup_linux("17.1009a");
    entry_win32_win64_linux("17.1002a");
    entry_win32_win64_linux("17.0930a");
    entry_win32_win64_linux("17.0327a");
    entry_win32_win64_linux("17.0325a");
    entry_win32_win64_linux("17.0215a");
    entry_win32_win64_linux("17.0115a");
    entry_win32_win64_linux("16.1230a");
    entry_win32_win64_linux("16.1210a");
    entry_win32_win64_linux("16.1125a");
    entry_win32_win64_linux("16.0927a");
    entry_win32_win64_linux("16.0920a");
    entry_win32_win64_linux("16.0904a");
    entry_win32_linux("16.0710a");
    entry_win32_linux("16.0705a");
    entry_win32_linux("16.0505a");
    entry_win32_linux("16.0420a");
    entry_win32_linux("16.0315a");
    entry_win32_linux("16.0305a");
    entry_win32_linux("16.0115a");
    entry_win32_linux("16.0110a");
    entry_win32_linux("15.1220a");
    entry_win32_linux("15.1025a");
    entry_win32_linux("15.0827a");
    entry_win32_linux("15.0825a");
    entry_win32_linux("15.0814a");
    entry_win32_linux("15.0810a");
    entry_win32_linux("15.0501a");
  ?>
  </p>

  <h3>Shader Edition</h3>
  <p>
  <?php
    entry_win32_win64_setup_linux_android("18.0416s");
    entry_win32_win64_linux_android("18.0410s");
    entry_win32_win64_linux_android("18.0330s");
    entry_win32_win64_linux_android("18.0315s");
    entry_win32_win64_linux_android("18.0310s");
    entry_win32_win64_linux("17.1222s");
    entry_win32_win64_linux("17.1218s");
    entry_win32_win64_linux("17.1124s");
    entry_win32_win64_linux("17.1115s");
    entry_win32_win64_linux("17.0618s");
    entry_win32_win64_linux("17.0615s");
  ?>
  </p>

  <br/>
  <hr/>

  <h2>Re-Volt v1.2</h2>
  <p>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2_changelog.txt" target="_blank">Changelog</a> |
    <a href="http://rv12.revoltzone.net/downloads/rv1.2_userdoc.txt" target="_blank">Documentation</a>
  </p>
  <p>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a15.0420_setup.exe" target="_blank">rv1.2a15.0420</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a15.0330_setup.exe" target="_blank">rv1.2a15.0330</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a15.0325_setup.exe" target="_blank">rv1.2a15.0325</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a15.0131_setup.exe" target="_blank">rv1.2a15.0131</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a15.0125_setup.exe" target="_blank">rv1.2a15.0125</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a14.0208_setup.exe" target="_blank">rv1.2a14.0208</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a13.0820_setup.exe" target="_blank">rv1.2a13.0820</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a13.0815_setup.exe" target="_blank">rv1.2a13.0815</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a12.1225.exe" target="_blank">rv1.2a12.1225</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a12.1102.exe" target="_blank">rv1.2a12.1102</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a12.0815.zip" target="_blank">rv1.2a12.0815</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a12.0802.zip" target="_blank">rv1.2a12.0802</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a12.0405.zip" target="_blank">rv1.2a12.0405</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a12.0107.exe" target="_blank">rv1.2a12.0107</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.1215.exe" target="_blank">rv1.2a11.1215</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0825.zip" target="_blank">rv1.2a11.0825</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0810.zip" target="_blank">rv1.2a11.0810</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0731.zip" target="_blank">rv1.2a11.0731</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0717.zip" target="_blank">rv1.2a11.0717</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0710.zip" target="_blank">rv1.2a11.0710</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0625.zip" target="_blank">rv1.2a11.0625</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0610.zip" target="_blank">rv1.2a11.0610</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0525.zip" target="_blank">rv1.2a11.0525</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0510.zip" target="_blank">rv1.2a11.0510</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0501.zip" target="_blank">rv1.2a11.0501</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0426.zip" target="_blank">rv1.2a11.0426</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2a11.0421.zip" target="_blank">rv1.2a11.0421</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0418.zip" target="_blank">rv1.2b11.0418</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0331.zip" target="_blank">rv1.2b11.0331</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0306.zip" target="_blank">rv1.2b11.0306</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0301.zip" target="_blank">rv1.2b11.0301</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0227.zip" target="_blank">rv1.2b11.0227</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0226.zip" target="_blank">rv1.2b11.0226</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0224.zip" target="_blank">rv1.2b11.0224</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0222.zip" target="_blank">rv1.2b11.0222</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv1.2b11.0208.zip" target="_blank">rv1.2b11.0208</a><br/>
  </p>

  <br/>
  <hr/>

  <h2>RV House</h2>
  <p>
    <a href="https://github.com/gk7huki/rvhouse" target="_blank">Sources</a> |
    <a href="http://rvhouse.revoltzone.net" target="_blank">Website (outdated)</a>
  </p>
  <p>
    <a href="http://rv12.revoltzone.net/downloads/rv_house_setup.exe" target="_blank">Windows (<?php echo $rv_house_build; ?>)</a><br/>
    <a href="http://rv12.revoltzone.net/downloads/rv_house_linux.tar.gz" target="_blank">Linux (<?php echo $rv_house_build; ?>)</a><br/>
  </p>

  <br/>
  <hr/>

  <h2>Our Re-Volt Pub</h2>
  <p>
    <a href="https://www.tapatalk.com/groups/ourrevoltpub/index.php" target="_blank">Website</a>
  </p>
  <p>
    <a href="orp/archive/index.html" target="_blank">Pub Archives</a>
  </p>

</body>
</html>
